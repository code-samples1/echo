# README #

### What is this repository for? ###

This repo demonstrates the practical application of some of more advanced topics, such as: synchronization, queues, async generators, etc.

I took the example from Python Asyncio [TCP echo server using streams](https://docs.python.org/3/library/asyncio-stream.html#tcp-echo-server-using-streams).

And had made it to the fully asynchronious server-client CLI app.

### How do I get set up? ###

We'll need:
	```
	python = "^3.8"
	aiofiles = "^0.7.0"
	```

FYI. From aiofiles we'll need wrap function. It implements [@functools.singledispatch](https://docs.python.org/3/library/functools.html#functools.singledispatch)

Clone this repo. 
Initialize new virtual env. 
Make sure you have aiofiles installed in venv.

Or use [virtual fish](https://virtualfish.readthedocs.io/en/latest/index.html) and [poetry](https://python-poetry.org)
	```
	$ vf new
	$ poetry install
	```



### How do I run the app? ###

You'll need a few terminals open.
Make sure that you are in the venv shell.


1. Run server
```
	$ cd echo
	$ python -m echo_server
	Serving on ('127.0.0.1', 8888)
```
2. Run client #1
```
	$ cd echo
	$ python -m echo_client
	Received "Login format: login [username]       login with specified username"
	$ login Thor
	Received "From <Vision>: Iron man awsome!"
	$ @Vision u a red, lol
	$ :q
	Received ":q"
	"Close the connection"
```

3. Run client #2
```
	$ cd echo
	$ python -m echo_client
	Received "Login format: login [username]       login with specified username"
	$ login Vision
	$ @Thor Iron man awsome!
	Received "From <Thor>: u a red, lol"
	$ :q
	Received ":q"
	"Close the connection"
```