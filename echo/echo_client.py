from __future__ import annotations
from typing import IO
import asyncio
import sys
import contextlib
from aiofiles.threadpool import wrap

from echo_utils import EchoWriter, EchoReader

# set to True to test synchronization
debug = False


class EchoClient:
    def __init__(self, debug=False) -> None:
        self.debug = debug

    async def send(self, message: IO[str]) -> None:
        print("CTRL + D to exit")
        write_queue: asyncio.Queue[bytes] = asyncio.Queue()

        reader, writer = await asyncio.open_connection("127.0.0.1", 8888)

        read_task = asyncio.create_task(
            self.__read(reader)
        )
        write_task = asyncio.create_task(
            EchoWriter(debug=self.debug).write(writer, write_queue)
        )
        stream_to_queue_task = asyncio.create_task(
            self.__put_to_queue(message, write_queue)
        )

        done, pending = await asyncio.wait([read_task, write_task, stream_to_queue_task], return_when=asyncio.FIRST_COMPLETED)

        print("Close the connection")
        for task in pending:
            task.cancel()
            with contextlib.suppress(asyncio.CancelledError):
                await task

    async def __read(self, reader: asyncio.StreamReader) -> None:
        async for message in EchoReader().get_message(reader):
            text = message.decode()
            print(f"Received {text!r}")
            if text == ":q":
                break

    async def __put_to_queue(self, message: IO[str], queue: asyncio.Queue[bytes]) -> None:
        loop = asyncio.get_event_loop()
        async for message in wrap(message, loop=loop):
            await queue.put(message.encode())


if __name__ == "__main__":
    asyncio.run(EchoClient(debug=debug).send(sys.stdin))
