from __future__ import annotations
from typing import Dict, Callable

import asyncio
import contextlib

from echo_utils import EchoWriter, EchoReader

# from pathlib import Path
# from starlette.config import Config
# current_dir = Path(__file__).parent
# config = Config(current_dir / ".env")
# debug = config("ECHOP_DEBUG", cast=bool, default=False)

debug = False


class EchoServer():
    def __init__(self, debug=False) -> None:
        self.debug = debug
        self.users: Dict[str, asyncio.Queue[bytes]] = {}

    async def serve(self) -> None:
        server = await asyncio.start_server(self.__handle_echo, "127.0.0.1", 8888)
        addr = server.sockets[0].getsockname() if server.sockets else "unknown"
        print(f"Serving on {addr}")
        async with server:
            await server.serve_forever()

    async def __handle_echo(self, reader: asyncio.StreamReader, writer: asyncio.StreamWriter) -> None:
        # keep track of method's args
        queue: asyncio.Queue[bytes] = asyncio.Queue()
        ctx = {
            "addr": str(writer.get_extra_info("peername")),
            "curr_nick": "",
        }
        await self.__handle_echo_helper(reader, writer, queue, ctx)

    async def __handle_echo_helper(
        self, reader: asyncio.StreamReader, writer: asyncio.StreamWriter, queue: asyncio.Queue[bytes], ctx: Dict[str, str]
    ):
        write_task = asyncio.create_task(
            EchoWriter(debug=self.debug).write(writer, queue))
        try:
            await self.__handle_commands(reader, queue, ctx)

        finally:
            print(0)
            curr_nick = ctx["curr_nick"]
            if curr_nick in self.users:
                del self.users[curr_nick]

            print("Close the connection")
            await queue.put(b"")
            with contextlib.suppress(asyncio.CancelledError):
                await write_task

    async def __handle_commands(self, reader: asyncio.StreamReader, queue: asyncio.Queue[bytes], ctx: Dict[str, str]) -> asyncio.StreamReader:

        addr = ctx["addr"]
        curr_nick = ctx["curr_nick"]

        await queue.put(b"Login format: login [username]       login with specified username")

        async for message in EchoReader().get_message(reader):
            text = message.decode()
            print(f"Received {text!r} from {addr!r}")
            if text.startswith("help"):
                if not curr_nick:
                    await queue.put(b"Usage: login [username]")
                    continue
            if text == ":q":
                await queue.put(message)
                break
            if text.startswith("login "):
                command, curr_nick = text.split(" ", 1)
                self.users[curr_nick] = queue
            elif text.startswith("@"):
                if not curr_nick:
                    await queue.put(b"Usage: login [username]")
                    continue
                target_nick, user_message = text.split(" ", 1)
                nick = target_nick[1:]
                if nick not in self.users:
                    await queue.put(b"User does not exist: " + nick.encode())
                    continue
                user_message = f"From <{curr_nick}>: {user_message}"
                await self.users[nick].put(user_message.encode())


if __name__ == "__main__":
    try:
        asyncio.run(EchoServer(debug=debug).serve())
    except KeyboardInterrupt:
        print("\nKeyboardInterrupt")
