from __future__ import annotations
from typing import AsyncIterator
import asyncio
import sys


class EchoReader:
    async def get_message(self, reader: asyncio.StreamReader) -> AsyncIterator[bytes]:
        data = b""
        try:
            while data := data + await reader.read(100):
                if b"\n" in data:
                    message, data = data.split(b"\n", 1)
                    yield message

        except ConnectionResetError:
            pass
        if data:
            print(False, data)
            yield data


class EchoWriter:
    def __init__(self, debug=False) -> None:
        self.debug = debug

    async def __write_per_byte(self, writer: asyncio.StreamWriter, message: bytes) -> None:
        # writes bytes one by one
        print("Writing bytes: ", end="")
        if not message.endswith(b"\n"):
            message += b"\n"

        for char in message:
            await asyncio.sleep(0.1)
            writer.write(bytes([char]))

            print(f"{char:0>2X}", end="")

            sys.stdout.flush()
            if char == 10:
                print()
        await writer.drain()

    async def write(self, writer: asyncio.StreamWriter, queue: asyncio.Queue[bytes]) -> None:
        try:
            while (message := await queue.get()) != b"":
                if self.debug:
                    await self.__write_per_byte(writer, message)
                else:
                    if not message.endswith(b"\n"):
                        message += b"\n"

                    writer.write(message)
                    await writer.drain()
        finally:
            await writer.drain()
            writer.close()
            # rather on 1 side, than on 2 sides w/ .close
            # like linux shutdown
            # writer.write_eof()
